package com.example.raihan.workingwithdatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by raihan on 28-Oct-17.
 */

public class DBHelper extends SQLiteOpenHelper {
    private static String dbName="myDbName.db";
    private static int DB_VERSION=5;

    public DBHelper(Context context) {
        super(context, dbName, null, DB_VERSION);
    }//constructor

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table MyTable(col1 number,col2 varchar(50),col3 varchar(50))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Add new table. Drop one existing table and re-create it.
        db.execSQL("create table MyTable2(col1 number,col2 varchar(50),col3 varchar(50))");
        db.execSQL("DROP TABLE IF EXISTS MyTable");
        onCreate(db);
    }
}
