package com.example.raihan.workingwithdatabase;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    DBHelper dbHelper;//= new DBHelper(getApplicationContext());
    SQLiteDatabase db;//= dbHelper.getWritableDatabase();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new DBHelper(getApplicationContext());
        db = dbHelper.getWritableDatabase();

        Button myButton1 = (Button) findViewById(R.id.button1);
        myButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            db.execSQL("CREATE TABLE IF NOT EXISTS Login(Username VARCHAR(50),Pass VARCHAR(50));");
            db.execSQL("INSERT INTO Login VALUES('admin','admin');");
            }
        });

        Button myButton2 = (Button) findViewById(R.id.button2);
        myButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Cursor resultSet = db.rawQuery("Select * from Login",null);
            resultSet.moveToFirst();

            String username = resultSet.getString(0);
            String password = resultSet.getString(1);

            TextView tv1 = (TextView) findViewById(R.id.textView1);
            TextView tv2 = (TextView) findViewById(R.id.textView2);

            tv1.setText(String.valueOf(username));
            tv2.setText(String.valueOf(password));
            }
        });
    }
}
